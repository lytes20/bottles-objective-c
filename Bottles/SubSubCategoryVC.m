//
//  SubSubCategoryVC.m
//  Bottles
//
//  Created by Gideon Bamuleseyo on 8/25/17.
//  Copyright © 2017 lytestech. All rights reserved.
//

#import "SubSubCategoryVC.h"
#import "AFNetworking.h"


@interface SubSubCategoryVC (){
    NSDictionary *inventoryDic;
    NSMutableArray *productNames;
    NSMutableArray *productPrices;
    NSMutableArray *productSizes;
    NSMutableArray *productImages;
    NSString *url;
    
    
}
@property (weak, nonatomic) IBOutlet UITableView *subSubTable;

@end

@implementation SubSubCategoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    url = @"";
   
    [self downloadTask];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return productNames.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subsubcat_cell"];
    NSDictionary *item =[productNames objectAtIndex:indexPath.row];
    
    
    
    
    UILabel *name = [cell viewWithTag:2];
    name.text = [productNames objectAtIndex:indexPath.row];
    // name.text = [NSMutableString stringWithFormat:@"%@", [item objectForKeyedSubscript:@"product_name"]];
    
    UILabel *size = [cell viewWithTag:3];
    size.text = [productSizes objectAtIndex:indexPath.row];
    // size.text = [NSMutableString stringWithFormat:@"%@", [item objectForKeyedSubscript:@"size"]];
    
    UILabel *price = [cell viewWithTag:4];
    price.text = [productPrices objectAtIndex:indexPath.row];
    // price.text = [NSMutableString stringWithFormat:@"%@", [item objectForKeyedSubscript:@"price"]];
    
    UIImageView *cellImage = [cell viewWithTag:10];
    NSURL *url = [NSURL URLWithString:[productImages objectAtIndex:indexPath.row]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    cellImage.image = [[UIImage alloc] initWithData:data];
    

    //[cellImage setImage:[item objectForKeyedSubscript:@"image"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIViewController *newVC = [self.storyboard  instantiateViewControllerWithIdentifier:@"viewproduct_vc"];
    [self.navigationController pushViewController:newVC animated:NO];
    
}

- (void)downloadTask{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager GET:@"https://popbottles.000webhostapp.com/getRed.php" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        productNames = [[NSMutableArray alloc] init];
        productPrices = [[NSMutableArray alloc] init];
        productSizes = [[NSMutableArray alloc] init];
        productImages = [[NSMutableArray alloc]init];
        inventoryDic = (NSDictionary *)responseObject;
        
        
        for (NSString *product in[inventoryDic valueForKey:@"product_name"]){
            [productNames addObject:product];
            
        }
        
        for (NSString *price in[inventoryDic valueForKey:@"price"] ){
            [productPrices addObject:price];
        }
        
        for (NSString *size in[inventoryDic valueForKey:@"size"]){
            [productSizes addObject:size];
        }
        for (NSString *imageUrl in[inventoryDic valueForKey:@"product_image"]){
            [productImages addObject:imageUrl];
        }
            
        
        
        [_subSubTable reloadData];
        NSLog(@"The array is: %@", productNames);
        // NSDictionary *dic = [NSJSONSerialization dataWithJSONObject:jsonData options:0 error:&error];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
