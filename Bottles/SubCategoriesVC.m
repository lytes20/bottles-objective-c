//
//  SubCategoriesVC.m
//  Bottles
//
//  Created by Gideon Bamuleseyo on 8/24/17.
//  Copyright © 2017 lytestech. All rights reserved.
//

#import "SubCategoriesVC.h"

@interface SubCategoriesVC (){
    NSMutableArray *subCategoryImages;
}

@end

@implementation SubCategoriesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    subCategoryImages = [NSMutableArray arrayWithObjects:[UIImage imageNamed:@"red03.jpg"], [UIImage imageNamed:@"rose03.jpg"], [UIImage imageNamed:@"white03.jpg"], [UIImage imageNamed:@"champagne.jpg"], [UIImage imageNamed:@"sparkling.jpg"], nil];
    
//    UICollectionView *collectionView = [self.view viewWithTag:10];
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout init];
    
//    CGFloat screenWidth = screenRect.size.width;
//    float cellWidth = screenWidth / 2.0;
//    layout.itemSize = CGSizeMake(cellWidth, cellWidth);
    
   
    

}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return subCategoryImages.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"subcat_cell" forIndexPath:indexPath];
    UIImageView *subCatImage = (UIImageView *)[cell viewWithTag:100];
    subCatImage.image = [subCategoryImages objectAtIndex:indexPath.row];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"i reach here bitches");
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 2.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth, cellWidth);
    
    return size;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UIViewController *newVC = [self.storyboard  instantiateViewControllerWithIdentifier:@"subsubcat_vc"];
    [self.navigationController pushViewController:newVC animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
