//
//  LogInVC.m
//  Bottles
//
//  Created by Gideon Bamuleseyo on 8/24/17.
//  Copyright © 2017 lytestech. All rights reserved.
//

#import "LogInVC.h"

@interface LogInVC ()

@end

@implementation LogInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}




- (IBAction)signInAction:(id)sender {
    UIViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"location_vc"];
    [self presentViewController:newVC animated:NO completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
