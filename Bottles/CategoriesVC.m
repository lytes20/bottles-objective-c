//
//  CategoriesVC.m
//  Bottles
//
//  Created by Gideon Bamuleseyo on 8/24/17.
//  Copyright © 2017 lytestech. All rights reserved.
//

#import "CategoriesVC.h"

@interface CategoriesVC ()

@property (strong, nonatomic) NSMutableArray *categoriesImage;

@end

@implementation CategoriesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _categoriesImage = [[NSMutableArray alloc] initWithObjects: [UIImage imageNamed:@"pop_spirits.jpg"], [UIImage imageNamed:@"pop_beers.jpg"], [UIImage imageNamed:@"pop_wines.jpg"], [UIImage imageNamed:@"pop_extras.jpg"], nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _categoriesImage.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"category_cell"];
    UIImageView *cellImage = [cell viewWithTag:1];
    cellImage.image = [_categoriesImage objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIViewController *newVC = [self.storyboard  instantiateViewControllerWithIdentifier:@"subcat_vc"];    
    [self.navigationController pushViewController:newVC animated:NO];

}

- (IBAction)shoppingCartButtonAction:(id)sender {
    UIViewController *anotherVC = [self.storyboard  instantiateViewControllerWithIdentifier:@"shopping_cart_nav"];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
