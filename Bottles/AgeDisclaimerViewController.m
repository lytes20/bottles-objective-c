//
//  AgeDisclaimerViewController.m
//  Bottles
//
//  Created by Gideon Bamuleseyo on 8/23/17.
//  Copyright © 2017 lytestech. All rights reserved.
//

#import "AgeDisclaimerViewController.h"

@interface AgeDisclaimerViewController ()

@end

@implementation AgeDisclaimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)yesAction:(id)sender {
    UIViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"login_vc"];
    [self presentViewController:newVC animated:NO completion:nil];
}
- (IBAction)noAction:(id)sender {
    UIApplication *myapp = [UIApplication sharedApplication];
    [myapp performSelector:@selector(suspend)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
