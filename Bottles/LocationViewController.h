//
//  LocationViewController.h
//  Bottles
//
//  Created by Gideon Bamuleseyo on 8/23/17.
//  Copyright © 2017 lytestech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface LocationViewController : UIViewController <CLLocationManagerDelegate>

@end
