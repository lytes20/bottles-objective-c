//
//  SubSubCategoryVC.h
//  Bottles
//
//  Created by Gideon Bamuleseyo on 8/25/17.
//  Copyright © 2017 lytestech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubSubCategoryVC : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
