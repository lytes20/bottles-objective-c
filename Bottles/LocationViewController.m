//
//  LocationViewController.m
//  Bottles
//
//  Created by Gideon Bamuleseyo on 8/23/17.
//  Copyright © 2017 lytestech. All rights reserved.
//

#import "LocationViewController.h"

@interface LocationViewController ()

@end

@implementation LocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)locationSelected:(id)sender {
    UIViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"category_nav"];
    [self presentViewController:newVC animated:NO completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
